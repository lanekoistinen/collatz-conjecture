# Return the Collatz Image
def collatzImg(n):
    return (3*n+1)*(n%2) + (n/2)*(1-n%2)

# Retrieve a natural number from user input
def getNaturalNum():
    try:
        n = int(input("n="))
        return n if n > 0 else getNaturalNum()
    except ValueError:
        return getNaturalNum()

# Test collatz conjecture
def runCollatzConj():
    n = getNaturalNum()
    count = int(0)
    while (n!=1):
        n=collatzImg(n)
        count+=1
        print(int(n), end=", ")
    print('[repeat 4, 2, 1]')
    print('Collatz Conjecture Held with', count, 'steps')


# Print title and run program
print('Test The Collatz Conjecture')
runCollatzConj()


    

